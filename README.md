# File Overview

Reports  of BaseOS/AppStream/EPEL packages that depend on content in CodeReady Builder.

- EL8 generated in a subscribed RHEL 8 image as of 2022.06.14
- EL9 generated in a subscribed RHEL 9 image as of 2022.06.13

## General Reports:

Ensure that BaseOS, AppStream, CodeReady Builder, and EPEL repositories are available and enabled.

- __crb-packages__:
	- `./gen-crb-packages`
- __rhel-on-crb__:
	- BaseOS and AppStream packages that depend on CRB/PT
	- `./gen-rhel-on-crb`
- __epel-on-crb__:
	- EPEL packages that depend on CRB/PT
	- `./gen-epel-on-crb`

## Raw JSON reports

Conversion from DNF plaintext to JSON

- __rhel-deps-raw.json__:
	- `../massage.py rhel-on-crb rhel-deps-raw.json`
- __epel-deps-raw.json__:
	- `../massage.py epel-on-crb epel-deps-raw.json`


## Final Reports

Remove all dependent packages that are from CRB/PT itself:

- __rhel-deps.json__:
	- `../cleanup.py rhel-deps-raw.json rhel-deps.json <releasever>`
- __epel-deps.json__:
	- `../cleanup.py epel-deps-raw.json epel-deps.json <releasever>`

Stripped of zero-depends packages:

- __FINAL_RHEL.json__:
	- `../finalize.py rhel-deps.json FINAL_RHEL.json`
- __FINAL_EPEL.json__:
	- `../finalize.py epel-deps.json FINAL_EPEL.json`

