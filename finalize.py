#!/bin/python3

import sys
import json

if len(sys.argv) != 3:
    exit("Please provide [input-clean] [output_final] files")

with open(sys.argv[1], "r") as file:
    dataset: dict = json.load(file)

store = dataset.copy()
for package in dataset.keys():
    if len(dataset[package]) == 0:
        store.pop(package)


with open(sys.argv[2], "w") as file:
    json.dump(store, file, indent=4)
