#!/bin/python3

import sys
import json

if len(sys.argv) != 3:
    exit("Please provide [dataset] [output_file]")

dataset = open(sys.argv[1], "r")
mut_output = dict()

prev_line = ""
key = None

for line in dataset:
    if ((line == "") or (line == "\n")):
        if (key is None):
            continue
        else:
            key = None
    if line[0].isalpha():
        if key is None:
            key = line.strip()
            mut_output[key] = []
        else:
            mut_output[key].append(line.strip())

with open(sys.argv[2], "w") as file:
    json.dump(mut_output, file, indent=4)

dataset.close()