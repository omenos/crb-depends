#!/bin/python3

import sys
import json

if len(sys.argv) != 4:
    exit("Please provide [input-raw] [output] files and releasever")

with open(sys.argv[1], "r") as file:
    dataset: dict = json.load(file)

for package in dataset.keys():
    for dep in dataset[package]:
        process_base = dep.split(":")
        process_pkg  = process_base[0][:-2]
        process_arch = process_base[1].split(f"el{sys.argv[3]}")[1].split(".")[-1]
        processed = f"{process_pkg}.{process_arch}"
        if processed in dataset.keys():
            new_list = dataset[package][:]
            new_list.remove(dep)
            dataset[package] = new_list


with open(sys.argv[2], "w") as file:
    json.dump(dataset, file, indent=4)
